#include <libudev.h>
#include <sys/epoll.h>
#include <fmt/format.h>
#include <vector>
#include <optional>
#include <csignal>
#include <unistd.h>
#include "device.hpp"
#include "numpad/numpad.hpp"

std::vector<std::pair<std::string_view, std::string_view>> const m_deviceIds =
{
  { "413c", "2005" },
//  { "04b3", "301b" },
//  { "046d", "c068" },
};

using DeviceList = std::vector<std::pair<std::string, std::unique_ptr<iodaemon::Device>>>;

struct epoll_event addFdToEpoll(int epoll_fd, int fd)
{
  struct epoll_event ev;
  ev.events = EPOLLIN;
  ev.data.fd = fd;
  epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &ev);
  return ev;
}

void removeFdFromEpoll(int epoll_fd, int fd)
{
  epoll_ctl(epoll_fd, EPOLL_CTL_DEL, fd, nullptr);
}

struct udev_monitor * startMonitorUdev(struct udev * udevHandle)
{
  struct udev_monitor * udevMonitor = udev_monitor_new_from_netlink(udevHandle, "udev");
  udev_monitor_filter_add_match_subsystem_devtype(udevMonitor, "input", nullptr);
  udev_monitor_enable_receiving(udevMonitor);
  return udevMonitor;
}

bool hasProperty(struct udev_device * device, std::string const & property, std::optional<std::string> const & expected = {})
{
  char const * p = udev_device_get_property_value(device, property.c_str());
  if (p == nullptr)
    return false;
  else if (!expected)
    return true;
  else
    return expected == p;
}

void stopMonitorDevice(struct udev_device * device, DeviceList & devices)
{
  const char* devPath = udev_device_get_devnode(device);

  devices.erase(std::remove_if(devices.begin(), devices.end(), [devPath](auto const & dev){ return dev.first == devPath; }), devices.end());
}

void startMonitorDevice(struct udev_device * device, DeviceList & devices)
{
  const char* devPath_c = udev_device_get_devnode(device);

  char const * vendor = udev_device_get_property_value(device, "ID_VENDOR_ID");
  char const * model = udev_device_get_property_value(device, "ID_MODEL_ID");

  bool valid(true);

  if (devPath_c == nullptr)
    valid = false;
  else if (std::string_view devPath = devPath_c; devPath.find("/dev/input/event") == devPath.npos)
    valid = false;
  else if (!vendor || !model)
    valid = false;
  else if (!hasProperty(device, "ID_INPUT_KEYBOARD", "1"))
    valid = false;
  else
  {
    for (auto const & [ v, m ] : m_deviceIds)
      valid = v == vendor && m == model;

    auto name = fmt::format("{}:{}", vendor, model);

    fmt::print("Device ({}): {} {}\n", valid, name, devPath_c);

    if (valid)
    {
      auto device = iodaemon::numpad::NumpadDevice::create(devPath_c);
      if (device)
        devices.emplace_back(std::make_pair(devPath_c, std::move(device)));
    }
  }
  udev_device_unref(device);
}

void getAttachedDevices(udev * udevHandle, DeviceList & devices)
{
  struct udev_enumerate * enumerate = udev_enumerate_new(udevHandle);
  udev_enumerate_add_match_subsystem(enumerate, "input");
  udev_enumerate_scan_devices(enumerate);
  udev_list_entry * udevices = udev_enumerate_get_list_entry(enumerate);

  udev_list_entry * item;
  udev_list_entry_foreach(item, udevices)
  {
    const char* name = udev_list_entry_get_name(item);
    udev_device * dev = udev_device_new_from_syspath(udevHandle, name);
    startMonitorDevice(dev, devices);
  }
  udev_enumerate_unref(enumerate);
}

void handleUdevEvent(udev_monitor * udevMonitor, DeviceList & devices)
{
  udev_device * dev = udev_monitor_receive_device(udevMonitor);
  if (dev)
  {
    std::string_view devAction = udev_device_get_action(dev);

    fmt::print("{}\n", devAction);

    if (devAction == "add")
      startMonitorDevice(dev, devices);
    else if (devAction == "remove")
      stopMonitorDevice(dev, devices);
  }
}

void start(int interruptFd)
{
  int epollFd = epoll_create1(0);

  udev * udevHandle = udev_new();
  udev_monitor * udevMonitor = startMonitorUdev(udevHandle);
  int udevMonitorFd = udev_monitor_get_fd(udevMonitor);

  addFdToEpoll(epollFd, interruptFd);
  addFdToEpoll(epollFd, udevMonitorFd);

  DeviceList devices;
  getAttachedDevices(udevHandle, devices);

  std::vector<epoll_event> event_queue(10);

  bool abort(false);
  while(!abort)
  {
    int n = epoll_wait(epollFd, event_queue.data(), event_queue.size(), -1);

    for (int i = 0; i < n; i++)
    {
      int fd = event_queue[i].data.fd;
      if (fd == interruptFd)
        abort = true;
      else if (fd == udevMonitorFd)
        handleUdevEvent(udevMonitor, devices);
    }
  }

  devices.clear();

  udev_unref(udevHandle);
}

namespace
{
  int _pipe[2];
  void handleSignal(int sig)
  {
    fmt::print("got sigint\n");
    close(_pipe[1]);
  }
}

int main(int argc, char * argv[])
{
  if (pipe(_pipe) != 0)
    return 1;

  std::signal(SIGINT, handleSignal);

  start(_pipe[0]);

  return 0;
}
