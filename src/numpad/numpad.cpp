#include "numpad.hpp"

#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>
#include <sys/epoll.h>
#include <vector>
#include <unistd.h>
#include <fcntl.h>

#include <fmt/format.h>

namespace iodaemon::numpad {

  /// Read data from evdev fd (keyboard), read data from uinput fd (leds), interrupt 

  std::vector<int> const keyCodes = {
    KEY_KP0, KEY_KP1, KEY_KP2, KEY_KP3, KEY_KP4, KEY_KP5, KEY_KP6, KEY_KP7, KEY_KP8, KEY_KP9,
    KEY_NUMLOCK, KEY_KPENTER, KEY_KPSLASH, KEY_KPDOT, KEY_KPASTERISK, KEY_KPMINUS, KEY_KPPLUS
  };

  epoll_event addFdToEpoll(int epoll_fd, int fd)
  {
    epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.fd = fd;
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &ev);
    return ev;
  }

  bool fd_set_blocking(int fd, bool blocking)
  {
    /* Save the current flags */
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        return false;

    if (blocking)
        flags &= ~O_NONBLOCK;
    else
        flags |= O_NONBLOCK;

    return fcntl(fd, F_SETFL, flags) != -1;
  }

  void readEvents(libevdev * dev, std::vector<input_event> & events)
  {
    while (true)
    {
      input_event ev;

      int rc = libevdev_next_event(dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);

      if (rc == LIBEVDEV_READ_STATUS_SUCCESS)
      {
        if (std::any_of(keyCodes.cbegin(), keyCodes.cend(), [code = ev.code] (int c) { return c == code; }))
          if (ev.value == 0 || ev.value == 1)
            events.push_back(ev);
      }
      else
        break;
    }
  }

  void readEvents(libevdev_uinput * uidev, std::vector<input_event> & events)
  {
    int rc;
    input_event ev;

    while((rc = read(libevdev_uinput_get_fd(uidev), &ev, sizeof(ev))) == sizeof(ev))
    {
      if (ev.type == EV_LED)
        events.push_back(ev);
    }
  }

  void sendEvents(libevdev * dev, std::vector<input_event> const & events)
  {
    for (auto const & ev : events)
      if (ev.type == EV_LED && ev.code == LED_NUML)
        libevdev_kernel_set_led_value(dev, ev.code, ev.value == 0 ? LIBEVDEV_LED_OFF : LIBEVDEV_LED_ON);
  }

  void sendEvents(libevdev_uinput * uidev, std::vector<input_event> const & events)
  {
    for (auto const & ev : events)
      libevdev_uinput_write_event(uidev, ev.type, ev.code, ev.value);
    if (!events.empty())
      libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 1);
  }

  void start(int interruptFd, libevdev_uinput * uidev, libevdev * dev)
  {
    int epollFd = epoll_create1(0);
    int devFd = libevdev_get_fd(dev);
    int uidevFd = libevdev_uinput_get_fd(uidev);

    fd_set_blocking(devFd, false);
    fd_set_blocking(uidevFd, false);

    addFdToEpoll(epollFd, interruptFd);
    addFdToEpoll(epollFd, libevdev_get_fd(dev));
    addFdToEpoll(epollFd, libevdev_uinput_get_fd(uidev));

    std::vector<epoll_event> event_queue(10);
    std::vector<input_event> events;

    libevdev_kernel_set_led_value(dev, LED_NUML, LIBEVDEV_LED_ON);

    bool abort(false);
    while (!abort)
    {
      events.clear();
      int n = epoll_wait(epollFd, event_queue.data(), event_queue.size(), -1);

      for (int i = 0; i < n; i++)
      {
        int fd = event_queue[i].data.fd;
        if (fd == interruptFd)
          abort = true;
        else if (fd == devFd)
        {
          readEvents(dev, events);
          sendEvents(uidev, events);
        }
        else if (fd == uidevFd)
        {
          readEvents(uidev, events);
          sendEvents(dev, events);
        }
      }
    }
    fmt::print("Terminating device\n");

    libevdev_kernel_set_led_value(dev, LED_NUML, LIBEVDEV_LED_OFF);

    libevdev_uinput_destroy(uidev);
    libevdev_free(dev);
    close(devFd);
    close(epollFd);
  }

  libevdev * createNumpadDevice()
  {
    libevdev * dev = libevdev_new();
    libevdev_set_name(dev, "NumpadDevice");
    libevdev_set_id_vendor(dev, 0x0000);
    libevdev_set_id_product(dev, 0x0001);

    for (auto key : keyCodes)
      libevdev_enable_event_code(dev, EV_KEY, key, nullptr);

    return dev;
  }

  std::unique_ptr<NumpadDevice> NumpadDevice::create(std::string const & devicePath)
  {
    libevdev * dev = createNumpadDevice();
    libevdev_uinput * uidev;
    int err = libevdev_uinput_create_from_device(dev, LIBEVDEV_UINPUT_OPEN_MANAGED, &uidev);

    if (err == 0)
    {
      fmt::print("Created uniput device\n");
      int fd = open(devicePath.c_str(), O_RDWR);

      if (fd >= 0)
      {
        err = libevdev_new_from_fd(fd, &dev);
        if (err == 0)
        {
          fmt::print("Created evdev\n");
          return std::unique_ptr<NumpadDevice>(new NumpadDevice(uidev, dev));
        }
        else
          fmt::print("Failed to create evdev device: {}\n", strerror(-err));

        close(fd);
      }
      else
        fmt::print("Failed to ppen device path: {}\n", devicePath);
      libevdev_uinput_destroy(uidev);
    }
    else
      fmt::print("Failed to create uinput device: {}\n", strerror(-err));
    return {};
  }

  NumpadDevice::NumpadDevice(libevdev_uinput * uidev, libevdev * dev)
  {
    fmt::print("Creating device\n");
    if (pipe(m_pipe) != -1)
      m_thread = std::thread(&start, m_pipe[0], uidev, dev);
  }

  NumpadDevice::~NumpadDevice()
  {
    fmt::print("Destroying device\n");

    close(m_pipe[1]);
    if (m_thread.joinable())
      m_thread.join();
  }

}
