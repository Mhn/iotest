#pragma once

#include <memory>
#include <thread>
#include "device.hpp"

struct libevdev;
struct libevdev_uinput;

namespace iodaemon::numpad {

  class NumpadDevice : public iodaemon::Device
  {
    public:
      static std::unique_ptr<NumpadDevice> create(std::string const & devicePath);
      ~NumpadDevice();

    private:
      NumpadDevice(libevdev_uinput * uidev, libevdev * dev);
      NumpadDevice(NumpadDevice const &) = delete;

    private:
      int m_pipe[2];
      std::thread m_thread;
  };

}
